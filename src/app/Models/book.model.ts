export interface Book extends baseClass{
    id: number;
    name: string;
    author: string;
    publishDate: Date;
    available: boolean;
}

export interface baseClass{
    creationDate: Date;
    CreatedBy: string;
}