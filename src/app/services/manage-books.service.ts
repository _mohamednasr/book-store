import { Injectable } from '@angular/core';
import { Book } from '../Models/book.model';

@Injectable({
  providedIn: 'root'
})
export class ManageBooksService{

  books: Book[] = [{
    id: 1,
    name: 'Book 1',
    author: "Author 1",
    publishDate: new Date("1-1-2019"),
    available: true,
    CreatedBy: 'Mohamed',
    creationDate: new Date('5-4-2020')
  },
  {
    id: 2,
    name: 'Book 2',
    author: "Author 2",
    publishDate: new Date("1-4-2019"),
    available: true,
    CreatedBy: 'Mohamed',
    creationDate: new Date('5-4-2020')
  },{
    id: 3,
    name: 'Book 3',
    author: "Author 2",
    publishDate: new Date('5-4-2020'),
    available: true,
    CreatedBy: 'Mohamed',
    creationDate: new Date('5-4-2020')
  }];

  constructor() { 
  }

  getBooks(){
    return this.books;
  }

  addBook(newBook: Book){
    this.books.push(newBook);
  }
}