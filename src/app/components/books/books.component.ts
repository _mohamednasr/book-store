import { Component, OnInit } from '@angular/core';
import { ManageBooksService } from 'src/app/services/manage-books.service';
import { Book } from 'src/app/Models/book.model';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {
 books: Book[];
  constructor(private booksService: ManageBooksService) { 
    this.books = booksService.getBooks();
  }

  ngOnInit() {
  }

}
