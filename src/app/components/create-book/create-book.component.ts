import { Component, OnInit } from '@angular/core';
import { ManageBooksService } from 'src/app/services/manage-books.service';
import { Book } from 'src/app/Models/book.model';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.scss']
})
export class CreateBookComponent implements OnInit {

  constructor(private bookService: ManageBooksService) { }

  ngOnInit() {
  }

  addNewBook(){
    let newBook: Book = {
      id: 5,
      name: 'Book 5',
      author: 'Author 5',
      publishDate: new Date('1-1-2020'),
      available: true,
      CreatedBy: 'user',
      creationDate: new Date('5-4-2020')
    }
    this.bookService.addBook(newBook);
  }

}
