import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './components/books/books.component';
import { CreateBookComponent } from './components/create-book/create-book.component';


const routes: Routes = [
  { path: '', pathMatch: "full", redirectTo: 'books' },
  { path: 'books', pathMatch: "full", component: BooksComponent },
  { path: 'new-book', pathMatch: "full", component: CreateBookComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
